# Instruções de execução

# Explicação de estruturação
O programa está dividido em 2 pastas.

A pasta "funcao", é para as funcoes que serão executadas.

Cada funcao está separada em uma pasta que tem o respectivo nome para organizar melhor
na hora de uma manutenção.

## Explicação do que cada função em cada pasta faz.
funcao>carrega 
carregaDados: Função para ler o arquivo TXT e gerar o arquivo final;
salvaDados: Função para estruturar a fora que será salva o arquivo final;

funcao>corrida
melhorVoltaC: Função para calcular a melhor volta da corrida;

funcao>media
mediaCorrida: Função para calcular a media da corrida de cada piloto;

funcao>piloto
melhorVoltaP: Função para calcular a melhor volta de cada piloto;

funcao>tempo
chegada: Função que calcula quanto tempo cada piloto chegou após o primeiro;

tela>resultado
resultado: Tela que mostra os resultados e executa as funções da pasta "funcao";

## Instruções para executar
Para executar, será necessario rodar a tela de "resultado.java", na pasta "tela>resultado";

## Comentários
Realmente existem funções do java que não me recordo como utilizar.
As boas práticas não foram aplicadas, por erro meu mesmo, acabei correndo para entrega e não aplicando.
Função basica de conversão de String para int - Não me recordo. Conversão aplicadas em todas as funções.
--Conversão correta, coletandp as informações erradas.
Em cada arquvio existe comentários explicando o que cada parte realiza, e as dificuldades encontradas.


