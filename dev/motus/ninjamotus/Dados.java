package dev.motus.ninjamotus;

public class Dados{

    private String chave = null;
    private String volta = null;
    private long tempo = 0;

    public String getChave() {
        return chave;
    }
    
    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getVolta() {
        return volta;
    }
    public void setVolta(String volta) {
        this.volta = volta;
    }

    public long getTempo() {
        return tempo;
    }
    public void setTempo(long tempo) {
        this.tempo = tempo;
    }
}