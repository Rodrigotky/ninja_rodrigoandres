
package dev.motus.ninjamotus;

import java.util.Date;

public class Volta {

    //Criando e identando as variaveis da Volta
    private String codigo = null;
    private String hora = null;
    private String piloto = null;
    private String numVolta = null;
    private String tempoVolta = null;
    private String velMediaVolta = null;
    
    private String melhorVolta = null;
    
    private long somaTempo = 0;
    
    
    public Volta(){
        
    }
    
    
    public Volta(
    
    String hora,
    String codigo,
    String piloto,
    String nVolta,
    String tempoVolta,
    String velMediaVolta
            
    ){
        
        this.hora = hora;
        this.codigo = codigo;
        this.piloto = piloto;
        this.numVolta = nVolta;
        this.tempoVolta = tempoVolta;
        this.velMediaVolta = velMediaVolta;
        
    }
    
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getHora() {
        return hora;
    }
   
    //Formatando a hora
    public Date getDateHora() {
        try {
            Date d = (Date) ResultadoFinal.formatterHora.parseObject(hora);
            return d;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    //Formatando a data para o resultado de tempo
    public Date getDateTempoVota() {
        try {
            Date d = (Date) ResultadoFinal.formatterMonutos.parseObject(this.tempoVolta);
            return d;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getPiloto() {
        return piloto;
    }

    public void setPiloto(String Piloto) {
        this.piloto = Piloto;
    }

    public String getNumVolta() {
        return numVolta;
    }

    public void setNVolta(String NVolta) {
        this.numVolta = NVolta;
    }

    public String getTempoVolta() {
        return tempoVolta;
    }

    public void setTempoVolta(String tempoVolta) {
        this.tempoVolta = tempoVolta;
    }

    public String getVmediaVolta() {
        return velMediaVolta;
    }

    public void setVmediaVolta(String vmediaVolta) {
        this.velMediaVolta = vmediaVolta;
    }

    
    
    public Long index(){
        long l = Long.parseLong(this.numVolta);
        return l;
    }
    
    public long getSomaTempo() {
        return somaTempo;
    }

    public void setSomaTempo(long somaTempo) {
        this.somaTempo = somaTempo;
    }
    
    //Formatação para formatar o tempo total
    public String getTempoTotal(){
        return ResultadoFinal.formatterMonutos.format(new Date(this.somaTempo));
    }
    
    //Formatação da data para fazer o calculo de media de voltas
    public double getMediaVolta(){
        Double d = new Double(new Date(this.somaTempo).getTime());
        return d / Integer.parseInt(numVolta);
    }
    
    public String getMelhorVolta() {
        return melhorVolta;
    }

    public void setMelhorVolta(String melhorVolta) {
        this.melhorVolta = melhorVolta;
    }
    
    
}
