package dev.motus.ninjamotus;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class ResultadoFinal {
    public static Format formatterHora = new SimpleDateFormat("HH:mm:ss.SSS");
    public static Format formatterMonutos = new SimpleDateFormat("mm:ss.SSS");

    public void gerarClassificacao() {
        try {

            String arquivo = "\\b\\s+\\W+";
            
            Map<String,Volta> dadosPiloto = new HashMap<String,Volta>();
            
            List<Volta> ordem = new ArrayList<Volta>();
            List<String> info = Files.readAllLines(new File("D:\\arquivo_de_entrada.txt").toPath(), Charset.forName("utf-8"));

            for (Iterator<String> iterator = info.iterator(); iterator.hasNext();) {
                String next = iterator.next();
                if (!next.startsWith("Hora")) {
                    String[] c = next.split(arquivo);
                    String piloto = c[1] + "-" + c[2];
                    ordem.add(new Volta(c[0],c[1],piloto , c[3], c[4], c[4]));
                }
            }
            
            List<Volta> posicao = new ArrayList<Volta>();            
            List<Dados> cMelhorVolta = new ArrayList<Dados>();
            
            int vlt = quantidadeVoltas(ordem);
            List<Volta> novaVlt = classificacao(ordem, vlt);
            for (Iterator<Volta> iterator = novaVlt.iterator(); iterator.hasNext();) {
                Volta v = iterator.next();
                try {
                    
                    Dados d = new Dados();
                    d.setChave(v.getPiloto());
                    d.setVolta(v.getNumVolta());
                    d.setTempo(v.getDateTempoVota().getTime());
                    
                    cMelhorVolta.add(d);
                    
                    String nomePiloto = v.getCodigo();
                    if(!dadosPiloto.containsKey(nomePiloto)){
                        v.setSomaTempo(v.getDateTempoVota().getTime());
                        dadosPiloto.put(nomePiloto,v);
                        posicao.add(v);
                    }else{
                        dadosPiloto.get(nomePiloto).setSomaTempo(dadosPiloto.get(nomePiloto).getSomaTempo() + v.getDateTempoVota().getTime());
                        if(dadosPiloto.get(nomePiloto).getDateTempoVota().getTime() > v.getDateTempoVota().getTime()){
                            dadosPiloto.get(nomePiloto).setMelhorVolta(v.getNumVolta());
                        }else{
                            dadosPiloto.get(nomePiloto).setMelhorVolta(dadosPiloto.get(nomePiloto).getNumVolta());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
            Collections.sort(cMelhorVolta, new Comparator<Dados>() {
                public int compare(Dados v1, Dados v2) {
                    int cont = new Long(v1.getTempo()).compareTo(v2.getTempo());
                    return cont;
                }
            });
            Dados d  = cMelhorVolta.get(0);
            FileWriter arq = new FileWriter("d:\\resultado.txt");
            PrintWriter gravarArq = new PrintWriter(arq);            

            System.out.printf("\nResultado Gravado com sucesso em \"d:\\resultado.txt\".\n");

            System.out.println("");
            System.out.println("---------------------------------------------------------------------");
            System.out.println("Voltas........: " + vlt);
            System.out.println("Corredores....: " + dadosPiloto.size());
            System.out.println("Melhor volta..: " + d.getVolta());
            System.out.println("");
            System.out.println("POSIÇÃO DE CHEGADA");
            System.out.println("---------------------------------------------------------------------");
            System.out.println("CHEGADA\tTEMPO       \tPILOTO/CÓDIGO\tVOLTAS \tTEMPO TOTAL\tTEMPO DEPOIS");

            gravarArq.printf("Resultado da Corrida\n");
            gravarArq.printf("---------------------------------------------------------------------\n");
            gravarArq.printf("Quantidade de Voltas........: " + vlt + "\n");
            gravarArq.printf("Quantidade de Corredores....: " + dadosPiloto.size() + "\n");
            gravarArq.printf("Melhor volta................: " + d.getVolta() + "\n");
            gravarArq.printf("\n");
            gravarArq.printf("Posição de chegada\n");
            gravarArq.printf("---------------------------------------------------------------------\n");
            gravarArq.printf("CHEGADA\tTEMPO       \tPILOTO/CÓDIGO\tVOLTAS \tTEMPO TOTAL\tTEMPO DEPOIS\n");

            int cont = 1;
            long tempPrimeiro = 0;
            for (Iterator<Volta> iterator = posicao.iterator(); iterator.hasNext();) {
                Volta v = iterator.next();
                
                if(tempPrimeiro == 0){
                    tempPrimeiro = v.getDateTempoVota().getTime();
                }   
              
                System.out.println(cont+ "º\t" + v.getHora() + "\t" + v.getPiloto() + "\t" + v.getNumVolta() + "\t" + v.getTempoTotal() + "\t" + (formatterMonutos.format(new Date(tempPrimeiro - v.getDateTempoVota().getTime())) ));
                gravarArq.printf(cont+ "º\t" + v.getHora() + "\t" + v.getPiloto() + "\t" + v.getNumVolta() + "\t" + v.getTempoTotal() + "\t" + (formatterMonutos.format(new Date(tempPrimeiro - v.getDateTempoVota().getTime())) ) + "\n" );
                cont++;              
            }

            System.out.println("");
            System.out.println("Bonus");
            System.out.println("---------------------------------------------------------------------");
            System.out.println("PILOTO/CÓDIGO\tMELHOR VOLTA\tMEDIA VOLTA\tTEMPO");

            gravarArq.printf("\n");
            gravarArq.printf("Bonus\n");
            gravarArq.printf("---------------------------------------------------------------------\n");
            gravarArq.printf("PILOTO/CÓDIGO\tMELHOR VOLTA\tMEDIA VOLTA\tTEMPO\n");

            for (Iterator<Volta> iterator = posicao.iterator(); iterator.hasNext();) {
                Volta v = iterator.next();
                
                if(tempPrimeiro == 0){
                    tempPrimeiro = v.getDateTempoVota().getTime();
                }             
                System.out.println( v.getPiloto() + "\t" +v.getMelhorVolta()+ "\t\t" + v.getMediaVolta() + "\t" + v.getHora());
                gravarArq.printf( v.getPiloto() + "\t" +v.getMelhorVolta()+ "\t\t" + v.getMediaVolta() + "\t" + v.getHora() + "\n");
                cont++;              
            }

            
            arq.close(); 

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private int quantidadeVoltas(List<Volta> lv) {
        Collections.sort(lv, new Comparator<Volta>() {
            public int compare(Volta v1, Volta v2) {
                int cont = v2.index().compareTo(v1.index());
                return cont;
            }
        } );
        return Integer.parseInt(lv.get(0).getNumVolta());
    }
    
    
    private List<Volta> classificacao(List<Volta> lv, int voltas) {
        
        List<Volta> vltCompletos = new ArrayList<Volta>();
        List<Volta> vltResto = new ArrayList<Volta>();
        List<Volta> novaVlt = new ArrayList<Volta>();
        
        for (Iterator<Volta> iterator = lv.iterator(); iterator.hasNext();) {
            Volta v = iterator.next();
            if(v.getNumVolta().equals(String.valueOf(voltas))){
                vltCompletos.add(v);
            }else{
                vltResto.add(v);
            }
        }
        
        Collections.sort(vltCompletos, new Comparator<Volta>() {
            public int compare(Volta v1, Volta v2) {
                return v1.getDateHora().compareTo(v2.getDateHora());
            }
        } );
        
        for (Iterator<Volta> iterator = vltCompletos.iterator(); iterator.hasNext();) {
            Volta next = iterator.next();
            novaVlt.add(next);
        }
        
        Collections.sort(vltResto, new Comparator<Volta>() {
            public int compare(Volta v1, Volta v2) {
                return v1.getDateHora().compareTo(v2.getDateHora());
            }
        } );
        
            for (Iterator<Volta> iterator = vltResto.iterator(); iterator.hasNext();) {
            Volta next = iterator.next();
            novaVlt.add(next);
        }
        
        
        return novaVlt;
    }
    
    public static void main(String[] args) {
        try {
            ResultadoFinal r = new ResultadoFinal();
            r.gerarClassificacao();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}